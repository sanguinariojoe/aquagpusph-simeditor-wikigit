# AQUAgpusph Pipeline editor

![snapshot](uploads/cc4bdbf57dce76bd5e398fc608a3904f/snapshot.png)

The AQUAgpusph Pipeline editor is a web service designed to make easier for users to customize the AQUAgpusph behavior, the so-called Pipeline.

**This editor is not designed to produce initial conditions**, i.e. the initial particles distribution to be loaded.
[Some hacks can be considered to this end](tools/execute_once) though.

## Usage

### Access the web service

The AQUAgpusph Pipeline editor can be accessed in the following website:

https://sanguinariojoe.gitlab.io/aquagpusph-simeditor

It is not needed to install nothing to create pipelines.
Conversely, you must install [AQUAgpusph](http://canal.etsin.upm.es/aquagpusph) to execute them.

This editor has been tested in the following browsers:

 * Firefox >= 80.0.1
 * Chromium >= 85.0.4183.102

### Create your own Pipeline

Unless a new pipeline shall be built from scratch, probably you want to start [loading a pipeline](load/home). Afterwards you can edit every single details of AQUAgpusph behavior:

 * [The variables considered](variables/home)
 * [The OpenCL definitions](definitions/home)
 * [The sets of particles to be loaded, and saved](sets/home)
 * [The tools to be executed](tools/home) each iteration.
 * [The settings](settings/home), like the [AQUAgpusph](http://canal.etsin.upm.es/aquagpusph) installation folder, or the files saving rate.

### Launch the simulation

When your pipeline is ready, you can [download it](save/home) as a ZIP file. Such ZIP file can be uncompressed anywhere, such that AQUAgpusph can be queried to execute the pipeline. For instance you can type:

`aquagpusph -i Main.xml`

which will launch a 3-D simulation using the Main.xml file inside the downloaded file.
In the command above it has been assumed that aquagpusph executable is in the system path, $PATH.
It has been also assumed that the command is executed in the same folder where the downloaded ZIP file was uncompressed.

## Contributing

There are several ways to contribute to the project:

 * [Edit the source code](contribute/code)
 * [Adding pipelines](contribute/pipelines)
 * [Documenting pipelines](contribute/pipelines_doc)
 * [Editing this wiki](contribute/wiki)