## How to submit the changes

Gitlab is great!
Unfortunately there is no way someone can create merge requests for the wiki, and therefore the conventional working procedure, i.e. forking, editing and submitting a merge request, is not possible on anymore.
To workaround this, a mirror of the wiki has been deployed in a different repository:

https://gitlab.com/sanguinariojoe/aquagpusph-simeditor-wikigit

Thus, you can fork that repository, clone the fork, work on the wiki, commit the changes, push and submit your merge requests, in the same way you would do for source code changes. Let's explain that in detail!

### 1.- Fork the mirror

You obviously needs to be logged with a [Gitlab](https://gitlab.com) account.
Then you can visit the forked repository web page you have recently created, and in the upper-left corner you can press Fork button.

![fork_snapshot](uploads/3fec5d4cfab95b723e05d473e5dcf202/fork_snapshot.png)

### 2.- Clone the fork

Now you can clone your new repository.
The information required to clone your repository can be found on the fork web page, under the "Clone" button.
For simplicity, you can clone that typing:

`git clone https://gitlab.com/<your_user>/aquagpusph-simeditor-wikigit.git`

where `<your_user>` shall be replaced by your user logging name.
The command above is creating a new "aquagpusph-simeditor-wikigit" folder, with the wiki inside.

### 3.- Work in the wiki

It is strongly recommended to install Gollum at the time to work in the wiki,

`gem install gollum rdoc`

You can automagically launch gollum typingthe following command in the repo folder:

`$(gem env | grep "USER INSTALLATION DIRECTORY" | sed 's/^.*: //')/bin/gollum`

(The command above is getting the gems installation path, using it as a prefix to find the gollum executable)

Now you can interactively edit the wiki in your browser, visiting the following URI:

http://localhost:4567/home

As it happens in the Gitlab wiki editor, when you save a page, a new commit is created in the repo, so you don't need to worry about that anymore.

### 4.- Push the changes

When you are ready, you can upload the changes to the repository, an operation called push, which can be carried out typing:

`git push origin master`

### 5.- Submit the merge request

If you visit now your forked repository web page, Gitlab will automatically guide you to create a new Merge Request.
You can learn more about Merge Requests [here](https://docs.gitlab.com/ee/user/project/merge_requests)

And what now?

Depending on your changes, the Merge Request can be accepted right away, or a discussion may arise, eventually requiring further changes.
In case you want to submit new changes, you can [recover your work](contribute/wiki#3.-_Work_in_the_wiki) without forking and cloning the repository again.

When your submitted merge request has been accepted and merged on the repository, the changes will not be instantaneously reflected here, but it would take sometime since some administrator must manually do the job.

## Request access to the main wiki

You can alternatively ask for writing rights on this very same wiki, which is obviously more convenient.
To this end, please submit an [issue](https://gitlab.com/sanguinariojoe/aquagpusph-simeditor/-/issues) clearly stating you want to get wiki edition rights.
Elaborate a bit the reasons for asking those rights.